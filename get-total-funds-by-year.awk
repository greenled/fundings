BEGIN {
  FS = ","
} 

{
  if ($4 in years) {
    years[$4]+=$5
  } else {
    years[$4]=$5
  }
}

END {
  for (year in years)
    printf year","years[year]"\n"
} 