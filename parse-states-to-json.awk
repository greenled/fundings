BEGIN {
  FS = ","
  print "[ "
} 

{
print "{ \"code\" : \"" $1 "\", \"raised\" : " $2 ", \"percent\" : " sprintf("%3.2f", ($2 / total_raised) * 100) ", \"intensity\" : " sprintf("%1.2f", ($2 / max_raised) * 0.8 + 0.2) " }"

  if (NR != lines_count) {
    print ","
  }
}

END {
  print " ]"
} 