BEGIN {
  FS = ","
} 

{
  split($4, splittedDate, "-")

  switch (splittedDate[3]) {
  case 93:
      $4 = 1993
      break
    case 98:
      $4 = 1998
      break
    case 99:
      $4 = 1999
      break
    case 00:
      $4 = 2000
      break
    case 01:
      $4 = 2001
      break
    case 02:
      $4 = 2002
      break
    case 03:
      $4 = 2003
      break
    case 04:
      $4 = 2004
      break
    case 05:
      $4 = 2005
      break
    case 06:
      $4 = 2006
      break
    case 07:
      $4 = 2007
      break
    case 08:
      $4 = 2008
      break
  }

  print $1","$2","$3","$4","$5
}