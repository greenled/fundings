BEGIN {
  FS = ","
}

{
  if ($4 == year) {
    if ($3 in states) {
      states[$3]+=$5
    } else {
      states[$3]=$5
    }
  }
}

END {
  for (state in states)
    printf state","states[state]"\n"
} 