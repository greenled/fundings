@load "readfile";

BEGIN {
  FS = ","
  print "[ "
} 

{
  states = readfile("generated-data/data-total-"$1".json")
  min_raised = readfile("generated-data/data-total-"$1"-min_raised")
  max_raised = readfile("generated-data/data-total-"$1"-max_raised")
  print "{ \"year\" : \"" $1 "\", \"raised\" : " $2 ", \"minRaised\" : "min_raised", \"maxRaised\" : "max_raised", \"states\" : "states" }"

  if (NR != lines_count) {
    print ","
  }
}

END {
  print " ]"
} 