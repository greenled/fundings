# Companies Funding in USA (1999-2008)

See https://greenled.gitlab.io/fundings

Final project for Information Visualization course

## About the visualization

Shows fund raising evolution in the USA for tech companies in the period 1999-2008, concentrated for each state. Dataset retrieved from [SpatialKey website](https://support.spatialkey.com/spatialkey-sample-csv-data/), originally provided by [TechCrunch](http://techcrunch.com).

## The team

- Dianelis Olivera Batista
- Lariza Portuondo Mallec
- Juan Carlos Mejías Rodríguez
- Yuri Ricardo Moragas Lugo
- José Miguel Fernández Pardo