#!/bin/bash

if [ -d "generated-data" ]; then
  rm -r generated-data
fi

mkdir generated-data

cat original-dataset.csv | gawk -f 0-remove-header.awk | gawk -f 1-normalize-fields-separation.awk | gawk -f 2-convert-EUR-to-USD.awk | gawk -f 3-convert-CAD-to-USD.awk | gawk -f 4-remove-irrelevant-fields.awk | gawk -f 5-remove-instances-with-missing-values.awk | gawk -f 6-remove-days-and-months.awk | gawk -f 7-remove-1993.awk > generated-data/filtered-data.csv

cat generated-data/filtered-data.csv | gawk -f get-total-funds-by-year.awk > generated-data/data-total.csv

for YEAR in 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008
do
  cat generated-data/filtered-data.csv | gawk -f get-total-funds-by-state-and-year.awk year="$YEAR" > generated-data/data-total-$YEAR.csv
done

# To JSON

for YEAR in 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008
do
  LINES_COUNT=($(wc -l generated-data/data-total-${YEAR}.csv))
  MIN_RAISED=($(cat generated-data/data-total-${YEAR}.csv | gawk 'BEGIN{FS = ","; min = -1}{if(min == -1 || $2 < min)min = $2}END{print min}'))
  MAX_RAISED=($(cat generated-data/data-total-${YEAR}.csv | gawk 'BEGIN{FS = ","}{if($2 > max)max = $2}END{print max}'))
  echo $MIN_RAISED > generated-data/data-total-$YEAR-min_raised
  echo $MAX_RAISED > generated-data/data-total-$YEAR-max_raised
  TOTAL_RAISED=($(cat generated-data/data-total-${YEAR}.csv | gawk 'BEGIN{FS = ","}{total += $2}END{print total}'))
  cat generated-data/data-total-$YEAR.csv | gawk -f parse-states-to-json.awk lines_count="$LINES_COUNT" min_raised="$MIN_RAISED" max_raised="$MAX_RAISED" total_raised="$TOTAL_RAISED" > generated-data/data-total-$YEAR.json
done

LINES_COUNT=($(wc -l generated-data/data-total.csv))
cat generated-data/data-total.csv | gawk -f parse-years-to-json.awk lines_count="$LINES_COUNT" > public/datasets/data.json
